import * as types from '../actions/actionTypes';
import usersJsonData from '../store/userData';
import productsJsonData from '../store/propertyData';
// import userDummyData from '../store/userDummyData';
// import initialState from './initialState';

const userDummyData = [
    {
        "id" : "ff80818160876da501608eeeb7a957e2",
        "broker" : false,
        "creation_date" : "2017-12-25T18:28:17.000Z",
        "disabled" : false,
        "email" : "firake.swpnl@gmail.com",
        "email_hash" : "78c40f2dc1a12f880f3c37e5ba4ad164",
        "email_verified" : false,
        "forgot_password_token" : null,
        "image_url" : null,
        "last_update_date" : "2017-12-25T18:30:55.000Z",
        "name" : "Swapnil Firake",
        "password" : "b56e0b4ea4962283bee762525c2d490f",
        "payment_date" : null,
        "phone" : "9970848287",
        "phone_verified" : false,
        "token_generation_date" : null,
        "user_type" : "SEEKER",
        "username" : null,
        "comments" : null,
        "allowed_int" : "9",
        "last_updated_by" : "<SYSTEM>",
        "verified_on" : "2017-12-25T18:30:02.000Z",
        "verified_by" : "magesh.k@nobroker.in",
        "state" : "GENUINE",
        "allowed_buy_int" : null,
        "corporate_account" : null
    },
    {
        "_id" : "5a50424b0bdbb46f7fe3aae1",
        "id" : "ff80818160876da501608eeb033c576a",
        "broker" : false,
        "creation_date" : "2017-12-25T18:24:14.000Z",
        "disabled" : false,
        "email" : "vipshah2010@gmail.com",
        "email_hash" : "dc308faa93aabace2bf08fe47b7faee5",
        "email_verified" : true,
        "forgot_password_token" : "a26edfb29655ce317a6e8591d0dd5343",
        "image_url" : null,
        "last_update_date" : "2017-12-26T10:24:50.000Z",
        "name" : "Vipul",
        "password" : "a23468cf3aee61a7cf04b514e05edf30",
        "payment_date" : null,
        "phone" : "9022554085",
        "phone_verified" : false,
        "token_generation_date" : "2017-12-26T10:24:50.000Z",
        "user_type" : "SEEKER",
        "username" : null,
        "comments" : null,
        "allowed_int" : null,
        "last_updated_by" : "vipshah2010@gmail.com",
        "verified_on" : "2017-12-26T10:11:40.000Z",
        "verified_by" : "2017-12-26T14:27:23.000Z",
        "state" : "GENUINE",
        "allowed_buy_int" : null,
        "corporate_account" : null
    },
    {
        "_id" : "5a50424b0bdbb46f7fe3aae3",
        "id" : "ff80818160792fe401608eea5e6b7d46",
        "broker" : false,
        "creation_date" : "2017-12-25T18:23:32.000Z",
        "disabled" : false,
        "email" : "sankpal.amruta121@gmail.com",
        "email_hash" : "c12bb737700755ece0fb2a5123b01d82",
        "email_verified" : false,
        "forgot_password_token" : null,
        "image_url" : null,
        "last_update_date" : "2017-12-25T18:30:07.000Z",
        "name" : "amruta",
        "password" : "835fcac7989b6e6157a58132c882dece",
        "payment_date" : null,
        "phone" : "8600233452",
        "phone_verified" : true,
        "token_generation_date" : null,
        "user_type" : "SEEKER",
        "username" : null,
        "comments" : null,
        "allowed_int" : "9",
        "last_updated_by" : "sankpal.amruta121@gmail.com",
        "verified_on" : "2017-12-25T18:25:07.000Z",
        "verified_by" : "magesh.k@nobroker.in",
        "state" : "GENUINE",
        "allowed_buy_int" : null,
        "corporate_account" : null
    },
    {
        "_id" : "5a50424b0bdbb46f7fe3aae8",
        "id" : "ff80818160792cc001608ee54c3e1813",
        "broker" : false,
        "creation_date" : "2017-12-25T18:18:00.000Z",
        "disabled" : false,
        "email" : "ansulag20@gmail.com",
        "email_hash" : "730c8e89103e3096e5fa7f1cf66d97ff",
        "email_verified" : false,
        "forgot_password_token" : null,
        "image_url" : null,
        "last_update_date" : "2017-12-25T18:18:27.000Z",
        "name" : "Ansul Aharwal",
        "password" : "f49c4e7de0207c8a5dfe19f43a3634a",
        "payment_date" : null,
        "phone" : "8918496605",
        "phone_verified" : false,
        "token_generation_date" : null,
        "user_type" : "SEEKER",
        "username" : null,
        "comments" : null,
        "allowed_int" : null,
        "last_updated_by" : "magesh.k@nobroker.in",
        "verified_on" : "2017-12-25T18:18:27.000Z",
        "verified_by" : "magesh.k@nobroker.in",
        "state" : "GENUINE",
        "allowed_buy_int" : null,
        "corporate_account" : null
    },
    {
        "_id" : "5a50424b0bdbb46f7fe3aaeb",
        "id" : "ff80818160792fe401608ee39c417c91",
        "broker" : false,
        "creation_date" : "2017-12-25T18:16:09.000Z",
        "disabled" : false,
        "email" : "tamal.cognizant@gmail.com",
        "email_hash" : "2605f1272577cb3ac62836f1d15bef85",
        "email_verified" : true,
        "forgot_password_token" : null,
        "image_url" : null,
        "last_update_date" : "2017-12-26T14:27:23.000Z",
        "name" : "Tamal",
        "password" : "1bff2c29ba889bb4d5694cfd099b710b",
        "payment_date" : null,
        "phone" : "8910468499",
        "phone_verified" : true,
        "token_generation_date" : null,
        "user_type" : "SEEKER",
        "username" : null,
        "comments" : null,
        "allowed_int" : "9",
        "last_updated_by" : "niraj.lal@nobroker.in",
        "verified_on" : "2017-12-26T14:27:23.000Z",
        "verified_by" : "niraj.lal@nobroker.in",
        "state" : "GENUINE",
        "allowed_buy_int" : null,
        "corporate_account" : null
    },
    {
        "_id" : "5a50424b0bdbb46f7fe3aaef",
        "id" : "ff80818160792cc001608ee056541771",
        "broker" : null,
        "creation_date" : "2017-12-25T18:12:35.000Z",
        "disabled" : false,
        "email" : "priyankaraonka@gmail.com",
        "email_hash" : "b3003f5966928ca9c48f2b919edd3a93",
        "email_verified" : false,
        "forgot_password_token" : null,
        "image_url" : null,
        "last_update_date" : "2017-12-25T18:13:19.000Z",
        "name" : "Priyanka",
        "password" : "b56e0b4ea4962283bee762525c2d490f",
        "payment_date" : null,
        "phone" : null,
        "phone_verified" : false,
        "token_generation_date" : null,
        "user_type" : "SEEKER",
        "username" : null,
        "comments" : null,
        "allowed_int" : null,
        "last_updated_by" : "magesh.k@nobroker.in",
        "verified_on" : "2017-12-26T14:27:23.000Z",
        "verified_by" : "2017-12-26T14:27:23.000Z",
        "state" : "NO_PHONE",
        "allowed_buy_int" : null,
        "corporate_account" : null
    },
    {
        "_id" : "5a50424b0bdbb46f7fe3aaf1",
        "id" : "ff80818160792cc001608ee01d3e1766",
        "broker" : null,
        "creation_date" : "2017-12-25T18:12:20.000Z",
        "disabled" : false,
        "email" : "paralleluniverse89@gmail.com",
        "email_hash" : "49fe3de4241c60e89c315a0196f572b8",
        "email_verified" : false,
        "forgot_password_token" : null,
        "image_url" : null,
        "last_update_date" : "2017-12-28T08:57:16.000Z",
        "name" : "Aaron",
        "password" : "b78ffa0136438964a9513ba601431e2f",
        "payment_date" : null,
        "phone" : "7002789637",
        "phone_verified" : false,
        "token_generation_date" : null,
        "user_type" : "SEEKER",
        "username" : null,
        "comments" : null,
        "allowed_int" : null,
        "last_updated_by" : "sumit.kumar@nobroker.in",
        "verified_on" : "2017-12-26T14:27:23.000Z",
        "verified_by" : "2017-12-26T14:27:23.000Z",
        "state" : "NON_CONTACTABLE",
        "allowed_buy_int" : null,
        "corporate_account" : null
    }
//  {
//     "id": "ff80818160c5f1330160c8544a843b8d",
//     "name": "Supreet Singh",
//     "creationDate": 1515186455000,
//     "lastUpdateDate": 1515189468433,
//     "verifyTimeInMillis": 13432,
//     "email": "supreetsingh.247@gmail.com",
//     "phone": "9833428809",
//     "corporateAccount": null,
//     "currentPlan": null,
//     "currentOwnerPlan": null,
//     "userState": "GENUINE",
//     "broker": false,
//     "verifiedBy": "SYSTEM",
//     "verifiedOn": 1515189469332,
//     "owner": false,
//     "allowedInt": "9",
//     "allowedBuyInt": "10",
//     "appActive": false,
//     "appLastActiveDate": null,
//     "aleToken": "09a02d59a7ed790adba5c02c00dc8a3a454b079b79dd418e2d02f486a2b8f3a6",
//     "emailVerified": true,
//     "phoneVerified": true
//  }
]

export function productsJson(state = [], action) {
    switch (action.type) {
        case 'CLEAR_PRODUCTS_JSON':
            return action.productsJson;
        case 'GETTING_PRODUCTS_JSON_SUCCESS':
            return productsJsonData;
        default:
            return state;
    }
}
export function usersJson(state = [], action) {
    switch (action.type) {
        case 'CLEAR_USERS_JSON':
            return action.usersJson;
        case 'GETTING_USERS_JSON_SUCCESS':
            return usersJsonData;
        default:
            return state;
    }
}

export function data(state = [], action) {
    switch (action.type) {
        case 'CLEAR_DATA':
            return action.data;
        case 'GETTING_DATA_SUCCESS':
            return userDummyData;
        default:
            return state;
    }
}

