import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import HomePage from './components/home/HomePage';
import DashboardContainer from './components/Dashboard/DashboardContainer';

function requireAuth(nextState, replace) {
  // if (1) {
  //   replace({
  //     pathname: 'about',
  //     query: {
  //       redirect: window.location.pathname,
  //     },
  //   });
  // }
}

export default (
  <Route path="/" component={App}>
      <IndexRoute component={HomePage}/>
      <Route path="dashboard" component={DashboardContainer}/>
      <Route path="*" component={HomePage} />
  </Route>
);