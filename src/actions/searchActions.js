import * as NetworkAdapter from '../components/common/NetworkAdapter';

export function clearProductsJson() { 
  return { 
    type: 'CLEAR_PRODUCTS_JSON', 
    productsJson: {},
  }; 
} 
export function gettingProductsJson(isLoading) { 
  return { 
    type: 'GETTING_PRODUCTS_JSON', 
    isLoading, 
  }; 
} 

export function gettingProductsJsonErrored(hasErrored) {
  return {
    type: 'GETTING_PRODUCTS_JSON_ERRORED',
    hasErrored,
  };
}

export function gettingProductsJsonSuccess(productsJson) {
  return {
    type: 'GETTING_PRODUCTS_JSON_SUCCESS',
    productsJson,
  };
}

export function getProductsJson(url) {
  return (dispatch) => {
    dispatch(gettingProductsJson(true));
    const myInit = NetworkAdapter.getHeaders('GET');
    NetworkAdapter.fetchData(dispatch, url, myInit, 
      gettingProductsJson, gettingProductsJsonSuccess, gettingProductsJsonErrored);
  };
}

export function clearUsersJson() { 
  return { 
    type: 'CLEAR_USERS_JSON', 
    usersJson: {},
  }; 
} 

export function gettingUsersJson(isLoading) { 
  return { 
    type: 'GETTING_USERS_JSON', 
    isLoading, 
  }; 
} 

export function gettingUsersJsonErrored(hasErrored) {
  return {
    type: 'GETTING_USERS_JSON_ERRORED',
    hasErrored,
  };
}

export function gettingUsersJsonSuccess(usersJson) {
  return {
    type: 'GETTING_USERS_JSON_SUCCESS',
    usersJson,
  };
}

export function getUsersJson(url) {
  return (dispatch) => {
    dispatch(gettingUsersJson(true));
    const myInit = NetworkAdapter.getHeaders('GET');
    NetworkAdapter.fetchData(dispatch, url, myInit, 
      gettingUsersJson, gettingUsersJsonSuccess, gettingUsersJsonErrored);
  };
}

export function clearData() { 
  return { 
    type: 'CLEAR_DATA', 
    data: {},
  }; 
} 

export function gettingData(isLoading) { 
  return { 
    type: 'GETTING_DATA', 
    isLoading, 
  }; 
} 

export function gettingDataErrored(hasErrored) {
  return {
    type: 'GETTING_DATA_ERRORED',
    hasErrored,
  };
}

export function gettingDataSuccess(data) {
  return {
    type: 'GETTING_DATA_SUCCESS',
    data,
  };
}

export function getData(url) {
  return (dispatch) => {
    dispatch(gettingData(true));
    const myInit = NetworkAdapter.getHeaders('GET');
    NetworkAdapter.fetchData(dispatch, url, myInit, 
      gettingData, gettingDataSuccess, gettingDataErrored);
  };
}

