import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as searchActions from '../../actions/searchActions';
//import {browserHistory} from 'react-router';
import toastr from 'toastr';
// import {loadAuthors} from '../../actions/authorActions';
import Header from '../common/Header';
import SearchContainer from '../Search/SearchContainer';
import LeftPanel from './DashboardViewItems/LeftPanel/LeftPanel';
import RightPanel from './DashboardViewItems/RightPanel/RightPanel';
import userDummyData from '../../store/userDummyData';

class DashboardContainer extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            data: props.data || [],
            selectedItem: null,
            dashboards: []
        };
        this.selectListItem = this.selectListItem.bind(this);
        this.onDashboardSave = this.onDashboardSave.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        console.log(this.state);
        if(nextProps.data){
            //Necessary to populate form when existing course is loaded directly.
            this.setState({data: nextProps.data})
        }
    }

    selectListItem (e, id) {
        let selectedObj = this.state.data.filter(dataPoint => dataPoint.id == id)[0];
        console.log(selectedObj);
        this.setState({ selectedItem: selectedObj });
    }

    onDashboardSave (name) {
        let dashboardList = this.state.dashboards;
        dashboardList.push(name);
        this.setState({dashboards: dashboardList});
    }
    
    render() {
        return (
            <div>
                <Header 
                    dashboards={this.state.dashboards}
                />
                <SearchContainer 
                    onDashboardSave={this.onDashboardSave}
                />
                <LeftPanel 
                    arrayList={this.state.data}
                    onSelect={this.selectListItem}
                />
                <RightPanel 
                    initiateNewSegment={this.initiateNewSegment}
                    selectedItem={this.state.selectedItem}
                />
            </div>
        );
    }
}

DashboardContainer.propTypes = {
    // course: PropTypes.object.isRequired,
    // authors: PropTypes.array.isRequired,
    // actions: PropTypes.object.isRequired
};

/*
* Pull in the React Router context so router is available on this.context.router
* Declared after class definition because it is a static property
* */
DashboardContainer.contextTypes = {
    router : PropTypes.object,
    store: React.PropTypes.object.isRequired
};

/*
* What part of store to expose to component as state
* Subscribes to store updates and returns an object
* */
function mapStateToProps(state, ownProps) {
    return {
        data: state.data,
        // authors: authorsFormattedForDropdown
    };
}

/*
* What actions we want to expose to components
* Takes dispatch as parameter
* Returns callback props that you want to pass down
* */
// function mapDispatchToProps(dispatch) {
//     return {
//         actions: bindActionCreators(searchActions, dispatch)
//     };
// }

export default connect(mapStateToProps)(DashboardContainer);
/*
* Connect creates container components. Wraps component so its connected to store.
* What parts of store connected to components as props.
* Also the actions we want to expose on props.
* Also prevents unnecessary renders. Only renders when connected part of store changes.
*/