import React from 'react';
import {Link} from 'react-router';
import styles from './rightPanel.css';
import Divider from 'material-ui/Divider';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';

const style = {
    listItem: {
        padding: 5
    },
    cardText: {
        paddingTop: 0
    }
}
// const convertDate = date => {return (new Date(date)).toString("MMM dd")};
const RightPanel = props => {
    const convertDate = date => (new Date(Date.parse(date))).toLocaleString();
    return (
        <div className="right-panel">
                <div className="header">Details</div>
                <Divider />
                {props.selectedItem ? 
                <div className="innerDiv">
                    <Card initiallyExpanded={true} className="card-personal-details">
                        <CardHeader
                            title="Personal Details"
                            // subtitle="Subtitle"
                            actAsExpander={true}
                            showExpandableButton={true}
                        />
                    <CardText expandable={true} style={style.cardText}>
                    <List>
                        <ListItem
                            primaryText={`Name : ${props.selectedItem.name}`}
                            disabled={true}
                            style={style.listItem}
                        />
                        {/* <Divider /> */}
                        <ListItem
                            primaryText={`Email : ${props.selectedItem.email}`}
                            disabled={true}
                            style={style.listItem}
                        />
                        <ListItem
                            primaryText={`Phone : ${props.selectedItem.phone}`}
                            disabled={true}
                            style={style.listItem}
                        />
                    </List>
                    </CardText>
                </Card>
                <Card initiallyExpanded={true} className="card-personal-details">
                        <CardHeader
                            title="Verification Details"
                            // subtitle="Subtitle"
                            actAsExpander={true}
                            showExpandableButton={true}
                        />
                    <CardText expandable={true} style={style.cardText}>
                    <List>
                        <ListItem
                            primaryText={`Phone Verified : ${props.selectedItem.phoneVerified ? 'Yes':'No'}`}
                            disabled={true}
                            style={style.listItem}
                        />
                        {/* <Divider /> */}
                        <ListItem
                            primaryText={`Email Verified: ${props.selectedItem.emailVerified ? 'Yes':'No'}`}
                            disabled={true}
                            style={style.listItem}
                        />
                        <ListItem
                            primaryText={`State : ${props.selectedItem.userState}`}
                            disabled={true}
                            style={style.listItem}
                        />
                        <ListItem
                            primaryText={`Verified By : ${props.selectedItem.verifiedBy}`}
                            disabled={true}
                            style={style.listItem}
                        />
                        <ListItem
                            primaryText={`Verified In (seconds) : ${props.selectedItem.verifyTimeInMillis/1000}`}
                            disabled={true}
                            style={style.listItem}
                        />
                        
                    </List>
                    </CardText>
                </Card>
                <Card initiallyExpanded={true} className="card-personal-details">
                        <CardHeader
                            title="Profile Timeline"
                            // subtitle="Subtitle"
                            actAsExpander={true}
                            showExpandableButton={true}
                        />
                    <CardText expandable={true} style={style.cardText}>
                    <List>
                        <ListItem
                            primaryText={`Created On: ${convertDate(props.selectedItem.creationDate)}`}
                            disabled={true}
                            style={style.listItem}
                        />
                        <ListItem
                            primaryText={`Verified In (seconds) : ${props.selectedItem.verifyTimeInMillis/1000}`}
                            disabled={true}
                            style={style.listItem}
                        />
                        <ListItem
                            primaryText={`Verified On: ${(new Date(Date.parse(props.selectedItem.verifiedOn))).toLocaleString()}`}
                            disabled={true}
                            style={style.listItem}
                        />
                        <ListItem
                            primaryText={`Last Update : ${(new Date(Date.parse(props.selectedItem.lastUpdateDate))).toLocaleString()}`}
                            disabled={true}
                            style={style.listItem}
                        />
                    </List>
                    </CardText>
                </Card>
                </div> : null }
        </div>
    );
}

export default RightPanel;