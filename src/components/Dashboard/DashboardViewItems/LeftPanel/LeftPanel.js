import React from 'react';
import {Link} from 'react-router';
import styles from './leftPanel.css';
import Divider from 'material-ui/Divider';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Checkbox from 'material-ui/Checkbox';

const style = {
    topCheckBox: {
        width: 20
    },
    listCheckBox: {
        position: 'absolute',
        top: 17,
        left: 15,
        width: 10
    },
    listItem: {
        paddingLeft: 40,
    }
}
const LeftPanel = props => {
    return (
        <div className="left-panel">
            <List>
        <Subheader><Checkbox style={style.topCheckBox} className="top-checkbox"/>User List</Subheader>
        <Divider />
        {props.arrayList && props.arrayList.length > 0 && props.arrayList.map((listItem) => (
        <div className="list-item">
            <Checkbox style={style.listCheckBox} />
            <ListItem
                primaryText={listItem.name}
                secondaryText={listItem.email}
                disabled={false}
                style={style.listItem}
                onClick={e => props.onSelect(e, listItem.id)}
            />
            <div className="list-phone">{listItem.phone}</div>
            <Divider />
        </div>
        ))}
        </List>
        </div>
    );  
}

export default LeftPanel;