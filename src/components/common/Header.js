import React, { Component } from 'react';
import {Link, IndexLink} from 'react-router';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';



class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {open: false};
        this.handleToggle = this.handleToggle.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleToggle () {
        this.setState({open: !this.state.open});
    }
    
    handleClose () {
        this.setState({open: false});
    }

    render() {
        return (
            <div>
                
                <AppBar
                    title="Project Manthan"
                    iconElementRight={
                        <IconMenu
                            iconButtonElement={
                            <IconButton><MoreVertIcon /></IconButton>
                            }
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                        >
                        <MenuItem primaryText="Refresh" />
                        <MenuItem primaryText="Help" />
                        <MenuItem primaryText="Sign out" />
                    </IconMenu>}
                    onLeftIconButtonClick={this.handleToggle}
                />
                <Drawer
                    docked={false}
                    width={250}
                    open={this.state.open}
                    onRequestChange={(open) => this.setState({open})}
                >
                    {this.props.dashboards && this.props.dashboards.map(dash => (
                        <MenuItem onClick={this.handleClose}>{dash}</MenuItem>
                        
                    ))}
                    <MenuItem onClick={this.handleClose}>RM Leads</MenuItem>
                </Drawer>
            </div>
        );
    }
}

export default Header;