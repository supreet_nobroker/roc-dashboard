import React, {PropTypes} from 'react';
import Header from './common/Header';
import {connect} from 'react-redux'

class App extends React.Component{
    render() {
        return (
            <div>
                {/* <Header /> */}
                {this.props.children}
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
    };
}
App.PropTypes = {
    children: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired 
};

export default connect(mapStateToProps)(App);