import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import InitiateSegment from './SearchViewItems/InitiateSegment/InitiateSegment';
import SearchPanel from './SearchViewItems/SearchPanel/SearchPanel';
import * as searchActions from '../../actions/searchActions';
import filtersJson from '../../store/filters';
import testData from '../../store/userData';

class SearchContainer extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            searchPanelOpen: false,
            andRows: [],
            orRows: [],
            deletedRows: [],
            jsonData: null,
            colValues: {
                col1value: '',
                col2value: '',
                col3value: '',
                col4value: ''
            },
            disabledButton: true,
            conditionArr: [],
            dashboardName: ''
        };
        this.openSearchPanel = this.openSearchPanel.bind(this);
        this.closeSearchPanel = this.closeSearchPanel.bind(this);
        // this.sumbitSearchQuery = this.sumbitSearchQuery.bind(this);
        this.addAndRow = this.addAndRow.bind(this);
        this.addOrRow = this.addOrRow.bind(this);
        this.removeRow = this.removeRow.bind(this);
        this.getData = this.getData.bind(this);
        this.setData = this.setData.bind(this);
        this.onChangeName = this.onChangeName.bind(this);
    }

    componentDidMount () {
        console.log(filtersJson);
        console.log(testData)
        if(!this.props.productsJson || (this.props.productsJson && 
            this.props.productsJson.length === 0)) {
                console.log()
                this.props.actions.getProductsJson('https://jsonplaceholder.typicode.com/posts/1');
        }
        if(!this.props.usersJson || (this.props.usersJson && 
            this.props.usersJson.length === 0)) {
                console.log()
                this.props.actions.getUsersJson('https://jsonplaceholder.typicode.com/posts/1');
        }
        
    }

    componentWillReceiveProps (nextProps) {
        if(nextProps.usersJson || nextProps.productsJson) {
            this.setState({disabledButton: false})
        }
    } 
    openSearchPanel (e, type) {
        let jsonData = null;
        if(type === 'user') {
            jsonData = this.props.usersJson;
        } else if (type === 'property') {
            jsonData = this.props.productsJson;
        }
        this.setState({
            searchPanelOpen: true,
            panelType: type,
            jsonData: jsonData
        });
        
    }
    closeSearchPanel () {
        this.setState({
            searchPanelOpen: false,
            andRows: [],
            orRows: []
        });
    }

    addAndRow (e) {
        const andRows = this.state.andRows;
        let newId;
        if(andRows.length > 0) {
            newId = andRows[andRows.length - 1] + 1;
        } else {
            newId = 0;
        }
        let andRow = JSON.parse(JSON.stringify(andRows));
        andRow.push(newId);
        this.setState({
            andRows: andRow
        }, function() {
            console.log(this.state.andRows);
        });
    }
    addOrRow () {
        const orRows = this.state.orRows;
        let newId;
        if(orRows.length > 0) {
            newId = orRows[orRows.length - 1] + 1;
        } else {
            newId = 0;
        }
        let orRow = JSON.parse(JSON.stringify(orRows));
        orRow.push(newId);
        this.setState({
            orRows: orRow
        }, function() {
            console.log(this.state.orRows);
        });
    }

    removeRow (type, id) {
        let row = type == 'and' ? this.state.andRows : this.state.orRows;
        row[id] = -1;
        if(type === 'and') {
            this.setState({
                andRows: row
            }, function() {
                console.log(this.state.andRows);
            })
        } else {
            this.setState({
                orRows: row
            }, function() {
                console.log(this.state.orRows);
            })
        }
        

    }

    setData (rowObj) {
        let arr = this.state.conditionArr;
        let exists = false;
        if(arr.length === 0) {
            arr.push(rowObj);
        } else {
            arr.map(arrVal => {
                if(arrVal.conditionType === rowObj.conditionType) {
                    if(arrVal.conditionId === rowObj.conditionId) {
                        exists = true;
                    }
                }
            })
            if(!exists) {
                arr.push(rowObj);
            }
        }
        this.setState({conditionArr: arr});
    }

    getData () {
        let arr = this.state.conditionArr;
        arr = arr.filter(arrObj => {
            if(arrObj.conditionType == 'and') {
                return this.state.andRows.indexOf(arrObj.conditionId) > -1 ? true : false;
            } else {
                return this.state.orRows.indexOf(arrObj.conditionId) > -1 ? true : false;
            }
        });
        console.log('Save Data Format: ');
        console.log(arr);
        this.closeSearchPanel();
        this.props.onDashboardSave(this.state.dashboardName);
        this.props.actions.getData('https://jsonplaceholder.typicode.com/posts/1');
    }

    onChangeName (e, value) {
        this.setState({dashboardName: value})
    }
    // removeAndRow () {
    //     this.setState({
    //         totalAndRows: totalAndRows - 1
    //     });
    // }
    render() {
        return (
            <div>
                <InitiateSegment 
                    openSearchPanel={this.openSearchPanel}
                    disabled={this.state.disabledButton}
                />
                <SearchPanel 
                    // initiateNewSegment={this.initiateNewSegment}
                    jsonData={this.state.jsonData}
                    filtersJson={filtersJson}
                    open={this.state.searchPanelOpen}
                    handleClose={this.closeSearchPanel}
                    sumbitSearchQuery={this.sumbitSearchQuery}
                    andRows={this.state.andRows}
                    addAndRow={this.addAndRow}
                    orRows={this.state.orRows}
                    addOrRow={this.addOrRow}
                    removeRow={this.removeRow}
                    deletedRows={this.state.deletedRows}
                    colValues={this.state.colValues}
                    onSubmit={this.getData}
                    onChangeEvent={this.setData}
                    onChangeName={this.onChangeName}
                    dashboardName={this.state.dashboardName}
                />
            </div>
        );
    }
}

SearchContainer.contextTypes = {
    // router : PropTypes.object,
    store: React.PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    console.log(state);
    return {
        productsJson: state.productsJson,
        usersJson: state.usersJson,
        productsJsonData: state.productsJson,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(searchActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchContainer);``