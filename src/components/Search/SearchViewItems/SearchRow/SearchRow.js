import React from 'react';
import styles from './searchRow.css';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';

const style = {
    customWidth: {
      width: 200,
    },
    textField: {
        position: 'absolute',
        top: 24
    },
    removeButton: {
        position: 'absolute',
        width: 50,
        minWidth: 20,
    }
  };


class SearchRow extends React.Component {
    
    constructor(props, context) {
        super(props, context);
        this.state = {
            type: props.type,
            rowId: props.rowId,
            col1Value: props.colValues.col1Value || '',
            col2Value: props.colValues.col2Value || '',
            col3Value: props.colValues.col3Value || '',
            col4Value: props.colValues.col4Value || '',
            col3Array: [],
            col4Type: null,
        };
        this.removeRow = this.removeRow.bind(this);
        this.handleChangeCol1 = this.handleChangeCol1.bind(this);
        this.handleChangeCol2 = this.handleChangeCol2.bind(this);
        this.handleChangeCol3 = this.handleChangeCol3.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
    }

    handleChangeCol1 (event, index, value) {
        this.setState({
            col1Value: value,
            col2Value: '',
            col3Value: '',
            col4Value: ''
        });
        const entitites = this.props.jsonData.subTypes.filter(subType => subType.name === value).entities;
    }

    handleChangeCol2 (event, index, value) {
        this.setState({col2Value: value});
        const entityObj = this.props.jsonData.subTypes
            .filter(subType => subType.name === this.state.col1Value)[0].entities
                .filter(entity => entity.name === value)[0];
        let col3Array = [];
        this.props.filtersJson.map(filter => {
            if(filter.type === 'date' && entityObj.date) {
                col3Array.push(...filter.fields);
            }
            if(filter.type === 'number' && entityObj.number) {
                col3Array.push(...filter.fields);
            }
            if(filter.type === 'string' && entityObj.string) {
                col3Array.push(...filter.fields);
            }
        });
        console.log(col3Array);
        this.setState({
            col3Array: col3Array,
            col3Value: '',
            col4Value: ''
        })
    }

    handleChangeCol3 (event, index, value) {
        this.setState({col3Value: value});
        this.props.filtersJson.map(filter => {
            if(filter.fields.indexOf(value) > -1) {
                this.setState({col4Type: filter.type, col4Value: ''});
           }
        });
    }

    handleChangeCol4 (type) {
        const rowObj = {
            col1: this.state.col1Value,
            col2: this.state.col2Value,
            col3: this.state.col3Value,
            col4: {
                type: type,
                value: this.state.col4Value
            },
            conditionType: this.state.type,
            conditionId: this.state.rowId,
        }
        this.props.onChangeEvent(rowObj);
    }

    onDateChange (e, date) {
        this.setState({col4Value: date}, () => this.handleChangeCol4('date'));
    }

    handleTextChange (e, value) {
        this.setState({col4Value: value}, () => this.handleChangeCol4('text')); 
    }

    removeRow () {
        this.props.removeRow(this.state.type, this.state.rowId);
    } 

    render () {
        const currentTypeObj = this.state.type === 'and' ? this.props.andRows : this.props.orRows;
        return (
            <div className="outer-row">
                {currentTypeObj && currentTypeObj.indexOf(this.state.rowId) > -1 ? 
                <div>
                <SelectField 
                    floatingLabelText="Select Type"
                    value={this.state.col1Value}
                    onChange={this.handleChangeCol1}
                    style={style.customWidth}
                    className="select-field"
                >
                    {this.props.jsonData && this.props.jsonData.subTypes.map((subType, index) => (
                         <MenuItem value={subType.name} primaryText={subType.name} />
                    ))}
                    
                </SelectField>

                {this.state.col1Value ? 
                <SelectField
                    floatingLabelText="Select SubType"
                    value={this.state.col2Value}
                    onChange={this.handleChangeCol2}
                    style={style.customWidth}
                    className="select-field"
                >
                {this.props.jsonData.subTypes.filter(subType => subType.name === this.state.col1Value)[0].entities.map((entity, index) => (
                     <MenuItem value={entity.name} primaryText={entity.name} />
                ))}
                </SelectField> : null
                }

                {this.state.col2Value ? 
                    <SelectField
                        floatingLabelText="Select Operator"
                        value={this.state.col3Value}
                        onChange={this.handleChangeCol3}
                        style={style.customWidth}
                        className="select-field"
                    >
                    {this.state.col3Array.map((col3, index) => (
                        <MenuItem value={col3} primaryText={col3} />
                    ))}
                    </SelectField> : null
                }
                {this.state.col4Type === 'date' ? 
                    <DatePicker 
                        hintText="Select Date" 
                        className="date-field" 
                        onChange={this.onDateChange}
                    />
                    :null
                }
                {(this.state.col4Type === 'number' || this.state.col4Type === 'string') ? 
                    <TextField
                        hintText="Enter Value"
                        defaultValue={this.state.col4Value}
                        style={style.textField}
                        onChange={this.handleTextChange}
                  />
                    : null
                }
                <RaisedButton
                    label="-"
                    secondary={true}
                    onClick={this.removeRow}
                    className="remove-btn"
                    style={style.removeButton}
                />
                </div>
                : null }
            </div>
        ) 
    }
       
}

export default SearchRow;