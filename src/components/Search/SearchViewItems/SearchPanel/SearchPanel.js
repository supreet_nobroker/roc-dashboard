import React from 'react';
import styles from './searchPanel.css';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import SearchRow from '../SearchRow/SearchRow';
import TextField from 'material-ui/TextField';

const customContentStyle = {
    width: '90%',
    maxWidth: 'none',
  };

  const style = {
    textField: {
        width: '20%',
        display: 'inherit'
    }
  }
const SearchPanel = props => {
    const actions = [
        <FlatButton
        label="Close"
        primary={true}
        onClick={props.handleClose}
        />,
        <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onClick={props.onSubmit}
        />,
    ];

    let andSearchRow = [];
    // andSearchRow = this.props.data
    //   .filter(item => this.state.deleted.indexOf(item.id) === -1)
    //   .map(item => {
    //     return (
    //       <Item key={item.id} id={item.id} onDelete={id => this.onDelete(id)}>
    //         {item.text}
    //       </Item>
    //     );
    //   });
    // if (props.andRows.length > 0) {      
    //     return props.andRows.map((andRow, index) => (
    //         andSearchRow.push(<SearchRow 
    //             rowId={i}
    //             type='and'
    //             removeRow={props.removeRow}
    //             // removeAndRow={props.removeAndRow}
    //         />)
    //     ));
    // }
    // for (let i = 0; i < props.totalAndRows; i++) {
    //     if(props.deletedRows.indexOf(i) == -1) {
    //         andSearchRow.push(<SearchRow 
    //             rowId={i}
    //             type='and'
    //             removeRow={props.removeRow}
    //             // removeAndRow={props.removeAndRow}
    //         />);
    //     }
    // }  
    // let orSearchRow = [];
    // for (let i = 0; i < props.totalOrRows; i++) {
    //     orSearchRow.push(<SearchRow 
    //         rowId={i}
    //         type='or'
    //         removeRow={props.removeRow}
    //         // removeAndRow={props.removeAndRow}
    //     />);
    // }    
    return (
            <Dialog
                title="Search Panel"
                actions={actions}
                modal={true}
                open={props.open}
                onRequestClose={props.handleClose}
                autoScrollBodyContent={true}
                contentStyle={customContentStyle}
            >
            <div className="outer-panel">
                <TextField
                    hintText="New Dashboard"
                    onChange={props.onChangeName}
                    value={props.dashboardName}
                    style={style.textField}
                />
                <FlatButton
                    label="Add AND Row"
                    primary={true}
                    onClick={props.addAndRow}
                />
                {props.andRows.length > 0 && props.andRows.map((andRow, index) => (
                    <SearchRow 
                        rowId={andRow}
                        type='and'
                        removeRow={props.removeRow}
                        panelType={props.panelType}
                        colValues={props.colValues}
                        jsonData={props.jsonData}
                        filtersJson={props.filtersJson}
                        andRows={props.andRows}
                        getData={props.getData}
                        onChangeEvent={props.onChangeEvent}
                        // removeAndRow={props.removeAndRow}
                    />
                ))}

                <FlatButton
                        label="Add OR Row"
                        primary={true}
                        onClick={props.addOrRow}
                    />
                {props.orRows.length > 0 && props.orRows.map((orRow, index) => (
                    <SearchRow 
                        rowId={orRow}
                        type='or'
                        removeRow={props.removeRow}
                        colValues={props.colValues}
                        jsonData={props.jsonData}
                        filtersJson={props.filtersJson}
                        orRows={props.orRows}
                        getData={props.getData}
                        onChangeEvent={props.onChangeEvent}
                        // removeAndRow={props.removeAndRow}
                    />
                ))}   
                </div>
            </Dialog>
    
    )    
}

export default SearchPanel;
