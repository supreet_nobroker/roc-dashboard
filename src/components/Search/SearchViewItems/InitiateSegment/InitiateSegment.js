import React from 'react';
import styles from './initiateSegment.css';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import IconMenu from 'material-ui/IconMenu';
import ContentAdd from 'material-ui/svg-icons/content/add';
import MenuItem from 'material-ui/MenuItem';


const InitiateSegment = props => (
    <IconMenu
      iconButtonElement={
      <FloatingActionButton className="initiate" secondary={true} disabled={props.disabled} >
        <ContentAdd />
      </FloatingActionButton>}
      anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
      targetOrigin={{horizontal: 'left', vertical: 'bottom'}}
    >
        <MenuItem primaryText="User" onClick={(e) => props.openSearchPanel(e, 'user')}/>
        <MenuItem primaryText="Property" onClick={(e) => props.openSearchPanel(e, 'property')}/>
    </IconMenu>
    
)

export default InitiateSegment;
