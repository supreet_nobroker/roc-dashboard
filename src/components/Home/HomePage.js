import React from 'react';
import {Link} from 'react-router';
import styles from './homepage.css';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
    floatingLabelFocusStyle: {
        color: '#303F9F',
    },
    inputField: {
        width: 360,
    },
    underlineStyle: {
        color: '#303F9F',
    },
    loginButton: {
        background: '#303F9F',
        width: 120,
        marginTop: 18,
        labelColor: '#ffffff',
    }
}

class HomePage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            passwordError: '',
            emailError: '',
        }
    }

    render() {
        return (
            <div className="cover">
                <div className="header-text">NoBroker</div>
                <div className="sub-header-text">Dashboard</div>
                <div className="login-card">
                    <div className="login-title">Login to continue</div>
                    <form>
                    <TextField
                        hintText="Email"
                        floatingLabelText="Email"
                        errorText={this.state.emailError}
                        floatingLabelFocusStyle={style.floatingLabelFocusStyle}
                        style={style.inputField}
                        underlineFocusStyle={style.underlineStyle}
                    /><br />
                    <TextField
                        hintText="Password"
                        floatingLabelText="Password"
                        type="password"
                        errorText={this.state.passwordError}
                        floatingLabelFocusStyle={style.floatingLabelFocusStyle}
                        style={style.inputField}
                        underlineFocusStyle={style.underlineStyle}
                    /><br />
                    <RaisedButton 
                        label="Login" 
                        style={style.loginButton} 
                        backgroundColor={style.loginButton.background}
                        labelColor={style.loginButton.labelColor}
                    />
                    </form>
                </div>
            </div>
        );
    }
}

export default HomePage;