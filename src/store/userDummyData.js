const userDummyData = {
    "id": "ff80818160c5f1330160c8544a882b8d",
    "name": "Mayur Patel",
    "creationDate": 1515189455000,
    "lastUpdateDate": 1515189469433,
    "verifyTimeInMillis": 14432,
    "email": "u185164@gmail.com",
    "phone": "9773953192",
    "corporateAccount": null,
    "currentPlan": null,
    "currentOwnerPlan": null,
    "userState": "GENUINE",
    "broker": false,
    "verifiedBy": "SYSTEM",
    "verifiedOn": 1515189469432,
    "owner": false,
    "allowedInt": "9",
    "allowedBuyInt": "10",
    "appActive": false,
    "appLastActiveDate": null,
    "aleToken": "09a02d59a7ed790adba5c02c00dc8a3a454b079b79dd418e2d02f486a2b8f3a6",
    "emailVerified": false,
    "phoneVerified": false
 }

 export default userDummyData;