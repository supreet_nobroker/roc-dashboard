const propertyData = 
{
    "type": "user",
    "subTypes": [
        {
            "name": "Property Attributes",
            "entities": [
                {
                    "name": "created_date",
                    "date": true,
                    "number": false,
                    "string": false
                },
                {
                    "name": "activated_date",
                    "date": true,
                    "number": false,
                    "string": true
                },
                {
                    "name": "propertyType",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "bhk",
                    "date": false,
                    "number": false,
                    "string": true
                },
                {
                    "name": "image_count",
                    "date": true,
                    "number": false,
                    "string": false
                },
                {
                    "name": "locality",
                    "date": false,
                    "number": false,
                    "string": true
                },
                {
                    "name": "active",
                    "date": false,
                    "number": false,
                    "string": true
                },
                {
                    "name": "activated_on",
                    "date": false,
                    "number": false,
                    "string": true
                }
            ]

        },
        {
            "name": "Property Events",
            "entities": [
                {
                    "name": "user_view",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "user_contacted",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "calls_to_tenant",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "calls_from_tenant",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "rent_edited",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "property_edited",
                    "date": true,
                    "number": true,
                    "string": false
                }
            ]
        }
    ]
}

export default propertyData;