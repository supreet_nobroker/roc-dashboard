const testData = 
{
    "type": "user",
    "subTypes": [
        {
            "name": "User Attributes",
            "entities": [
                {
                    "name": "creationDate",
                    "date": true,
                    "number": false,
                    "string": false
                },
                {
                    "name": "currentTenantPlan",
                    "date": true,
                    "number": false,
                    "string": true
                },
                {
                    "name": "noOfUniqueContacts",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "broker",
                    "date": false,
                    "number": false,
                    "string": true
                },
                {
                    "name": "name",
                    "date": false,
                    "number": false,
                    "string": true
                },
                {
                    "name": "phone",
                    "date": false,
                    "number": false,
                    "string": true
                },
                {
                    "name": "email",
                    "date": false,
                    "number": false,
                    "string": true
                }
            ]

        },
        {
            "name": "Events",
            "entities": [
                {
                    "name": "property_view",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "property_contacted",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "last_call_received",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "call_not_received",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "called_to_owner",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "SELLER_RELAX_payment_initiated",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "SELLER_RELAX_payment_failed",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "SELLER_RELAX_payment_aborted",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "SELLER_RELAX_payment_converted",
                    "date": true,
                    "number": true,
                    "string": false
                },
                ,
                {
                    "name": "SELLER_RELAX_payment_initiated",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "SELLER_RELAX_payment_failed",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "SELLER_RELAX_payment_aborted",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "SELLER_RELAX_payment_converted",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "ra_started",
                    "date": true,
                    "number": true,
                    "string": false
                },
                {
                    "name": "ra_converted",
                    "date": true,
                    "number": true,
                    "string": false
                }
            ]

        }
    ]
}

export default testData;