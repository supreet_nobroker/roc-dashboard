
const filtersJson = [
    {
        "type": "date",
        "fields": ["before", "after", "between", "on", "firstOccurDateBefore", 
            "firstOccurDateAfter", "lastOccurDateBefore", "lastOccurDateAfter"]
    },
    {
        "type": "number",
        "fields": ["countLessThan", "countMoreThan", "countBetween", "countExactlyEqualsTo", "countNon-zero", 
            "countZero"]
    },
    {
        "type": "string",
        "fields": ["exist", "doesNotExist", "containsFollowingWords", "exactlyMatches", "startsWith", 
            "endsWith"]
    }
]
export default filtersJson;