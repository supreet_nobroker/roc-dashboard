import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux'; //helps us access store from components
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import { applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import './styles/styles.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/toastr/build/toastr.min.css';
import Pusher from 'pusher-js';

// store.dispatch(loadCourses());
// store.dispatch(loadAuthors());
injectTapEventPlugin();
const store = configureStore(applyMiddleware(thunk));


/*This is the provider component which attaches app to store. Used at root. Wraps entire application.*/

render(
    <MuiThemeProvider>
        <Provider store = {store}>
            <Router history={browserHistory} routes={routes} />
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('app')
);


